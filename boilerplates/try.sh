#!/usr/bin/env sh

# POSIX compatible functions to enable reliable scripts
# call script using 'sh try.sh' as sourcing the file with '. try.sh' may exit your shell

shout() { echo "$0: $*" >&2; }
die() { shout "$*"; exit 111; }
try() { "$@" || die "cannot $*"; }

# example usage with optional logger

# logger "Starting boilerplate"
try cd /some/place
try tar xfv /missing/file.tar
# logger "Finished boilerplate"

true
