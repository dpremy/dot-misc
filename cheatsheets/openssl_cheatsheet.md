# openssl

## Cert Generation

```bash
# Generate a new CSR with private key
openssl req -new -newkey rsa:2048 -nodes -keyout private.key -out csr.csr

# Generate a CSR for an existing private key
openssl req -new -key private.key -out csr.csr

# Generate a CSR based on an existing certificate
openssl x509 -x509toreq -in cert.crt -out csr.csr -signkey private.key

# Generate a self-signed certificate
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout private.key -out cert.crt

# Remove a passphrase from a private key
openssl rsa -in private.pem -out newprivate.pem
```

## Checking and Verifying

```bash
# Output a CSRs content
openssl req -text -noout -verify -in cert.csr

# Check a private key is valid
openssl rsa -check -in private.key

# Output a certficates content
openssl x509 -text -noout -in cert.crt

# Output a PKCS#12 file (.pfx or .p12) content
openssl pkcs12 -info -in cert.p12

# Verify a csr, key, and cert are all from the same request by verifying the modulus
csr_file=cert.csr
key_file=cert.key
cert_file=cert.cer
openssl req -noout -modulus -in ${csr_file}; openssl rsa -noout -modulus -in ${key_file}; openssl x509 -noout -modulus -in ${cert_file}

# Output a TLS certificate
echo "" | openssl s_client -connect www.server.com:443
echo "" | openssl s_client -showcerts -connect www.server.com:443 | openssl x509 -text -noout
```

## Converting

```bash
# Convert a PKCS#12 file (.pfx .p12) containing a private key and cert to PEM
#   add -nocerts to output only the private key
#   add -nokeys to output only the cert
openssl pkcs12 -nodes -in keyStore.pfx -out keyStore.pem

# Convert a PEM and private key to PKCS#12 (.pfx .p12)
openssl pkcs12 -export -out cert.pfx -inkey private.key -in cert.crt -certfile ca.crt

# Convert a DER file (.crt .cer .der) to PEM
openssl x509 -inform der -in cert.cer -out cert.pem

# Convert a PEM file to DER
openssl x509 -outform der -in cert.pem -out cert.der
```
