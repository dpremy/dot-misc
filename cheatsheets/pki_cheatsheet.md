# PKI for Linux Client Systems

---

## RHEL Distros

```bash
# RHEL/CentOS 6+
cd /usr/share/pki/ca-trust-source/anchors
sudo curl -o INTERMEDIATE-CA.crt http://pki.site.com/INTERMEDIATE-CA.pem
sudo curl -o ROOT.crt http://pki.site.com/ROOT.pem
sudo curl -o DigiCertSHA2SecureServerCA.crt.pem https://dl.cacerts.digicert.com/DigiCertSHA2SecureServerCA.crt.pem
sudo update-ca-trust extract
```

## Debian Distros

```bash
# Debian 9+
cd /usr/local/share/ca-certificates
sudo curl -o INTERMEDIATE-CA.crt http://pki.site.com/INTERMEDIATE-CA.pem
sudo curl -o ROOT.crt http://pki.site.com/ROOT.pem
sudo curl -o DigiCertSHA2SecureServerCA.crt.pem https://dl.cacerts.digicert.com/DigiCertSHA2SecureServerCA.crt.pem
sudo update-ca-certificates -v
```
