# tar

## Backups with CACHEDIR

Everyone has their own way of doing tar backups, here is mine.

```bash
#!/usr/bin/env bash

# start a counter for any errors
declare -i ERRORS=0

# to exclude any directory from the tar file create CACHEDIR.TAG with a single line 'Signature: 8a477f597d28d172789f06886806bc55'
#   this is used by --exclude-caches-under to exclude all files and folders from which the CACHEDIR.TAG file exists

tar --exclude-caches-under --exclude-vcs-ignores -cjf /backups/home.tar.bz2 -C / home/
ERRORS+=$?
tar --exclude-caches-under --exclude-vcs-ignores -cjf /backups/website.tar.bz2 -C /var/www/ website/
ERRORS+=$?
tar --exclude-caches-under --exclude-vcs-ignores -cjf /backups/etc.tar.bz2 -C / etc/
ERRORS+=$?

# exit using the combined total of any errors
exit ${ERRORS}
```
