# tcpdump

---

Source: <https://gitlab.com/dpremy/dot-misc/-/blob/master/cheatsheets/tcpdump_cheatsheet.md>

```bash
# list all nics
tcpdump -D

# no timestamps, no name or port conversions, semi verbose, and interface eth1
tcpdump -tttt -nn -vv -i eth1

# dump all ASCII traffic
tcpdump -A

# write pcap
tcpdump -w <filename>.pcap

# read pcap
tcpdump -r <filename>.pcap

# all traffic to/from specific hosts
tcpdump host 192.168.0.1 or host 192.168.0.2

# all traffic from network
tcpdump net 10.1.1.0/24

# all traffic except from host or net
tcpdump not net 192.168.1 and not host 192.168.0.254

# all traffic on a port range, or a single port, with no name or port conversions
tcpdump portrange 7700-7750 or port 7800 -nn

# wait for 2000 packets, then list top 20 clients (DOS/DDOS)
tcpdump -nn -c 2000 | awk '{print $2}' | cut -d. -f1-4 | sort -n | uniq -c | sort -rn | head -n20
```
